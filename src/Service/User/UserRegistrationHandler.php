<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;

class UserRegistrationHandler
{
    private UserRepository $userRepository;
    private UserPasswordUpdater $passwordUpdater;

    public function __construct(UserRepository $userRepository, UserPasswordUpdater $passwordUpdater)
    {
        $this->userRepository = $userRepository;
        $this->passwordUpdater = $passwordUpdater;
    }

    public function register(User $user, string $locale)
    {
        $user->setLocale($locale);
        $this->passwordUpdater->updatePassword($user);
        $this->userRepository->save($user);
    }
}