<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

class UserAuthenticator
{
    private TokenStorageInterface $tokenStorage;
    private SessionInterface $session;
    private UserRepository $userRepository;

    public function __construct(TokenStorageInterface $tokenStorage, SessionInterface $session, UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->userRepository = $userRepository;
    }

    public function authenticate(UserInterface $user)
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), 'app_user_provider', $user->getRoles());
        $this->tokenStorage->setToken($token);
        $this->session->set(User::FIRST_LOGIN_FLAG, true);
        $this->userRepository->updateLoginData($user);
    }
}