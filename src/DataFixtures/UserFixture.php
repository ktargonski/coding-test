<?php
/**
 * IGNORE FIXTURE FILES
 */

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\User\UserPasswordUpdater;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{

    private UserPasswordUpdater $passwordUpdater;

    public function __construct(UserPasswordUpdater $passwordUpdater)
    {
        $this->passwordUpdater = $passwordUpdater;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();

        $user
            ->setTitle(User::TITLE_MR)
            ->setName('test')
            ->setEmail('test@ecocode.de')
            ->setLocale('en')
            ->setPlainPassword('test');

        $this->passwordUpdater->updatePassword($user);

        $manager->persist($user);
        $manager->flush();
    }
}
