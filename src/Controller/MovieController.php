<?php

namespace App\Controller;

use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends AbstractController
{
    public function index(MovieRepository $movieRepository): Response
    {
        return $this->render('movie-list.html.twig', ['movies' => $movieRepository->getMovieList()]);
    }
}