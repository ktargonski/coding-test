<?php

namespace App\Controller\User;

use App\Service\User\UserAuthenticator;
use App\Service\User\UserRegistrationHandler;
use App\Entity\User;
use App\Form\User\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends AbstractController
{
    public function create(Request $request, UserRegistrationHandler $registrationHandler, UserAuthenticator $userAuthenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $registrationHandler->register($user, $request->getLocale());
                $userAuthenticator->authenticate($user);

                return $this->redirectToRoute('app_homepage');
            }
        }

        return $this->render('user/registration.html.twig', ['form' => $form->createView()]);
    }
}
